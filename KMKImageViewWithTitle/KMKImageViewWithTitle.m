//  Copyright (c) 2015, Yauheni Klishevich.
//  Released under the MIT license.

#import "KMKImageViewWithTitle.h"

#import "NSString+KMKUtils.h"

#import "KMKImageViewWithTitleLayoutInfo.h"
#import "KMKImageViewWithTitleLayoutInfo.h"


@implementation KMKImageViewWithTitle

#pragma mark
#pragma mark Class methods

+ (CGSize)preferredSizeForDataSource:(KMKImageViewWithTitleLayoutParams *)params size:(CGSize)size {
    KMKImageViewWithTitleLayoutInfo *layoutInfo = [self layoutInfo_imageViewWithTitle_forParams:params
                                                                                   viewSize:size];
    return layoutInfo.contingentRightSize;
}

#pragma mark
#pragma mark Allocation

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self init_common];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        [self init_common];
    }
    return self;
}

#pragma mark
#pragma mark Protected

- (CGSize)sizeThatFits:(CGSize)size {
    KMKImageViewWithTitleLayoutInfo *layoutInfo = [[self class] layoutInfo_imageViewWithTitle_forParams:[self layoutParams]
                                                                                               viewSize:self.frame.size];
    return layoutInfo.contingentRightSize;
}

- (void)layoutSubviews {
    [super layoutSubviews];    
    KMKImageViewWithTitleLayoutInfo *layoutInfo = [[self class] layoutInfo_imageViewWithTitle_forParams:[self layoutParams]
                                                                                               viewSize:self.frame.size];
    self.imageView.frame = layoutInfo.imageFrame;
    self.titleLabel.frame = layoutInfo.titleFrame;
}

#pragma mark
#pragma mark Private

/**
 contingent right size is not larger than the specified size
 The width is left the same.
 */
+ (KMKImageViewWithTitleLayoutInfo *)layoutInfo_imageViewWithTitle_forParams: (KMKImageViewWithTitleLayoutParams *)params
                                                                    viewSize: (CGSize)size {
    
    KMKImageViewWithTitleLayoutInfo *layoutInfo = [KMKImageViewWithTitleLayoutInfo new];
    
    CGRect titleFrame = CGRectZero;
    titleFrame.size = [params.title kmk_multilineSizeConstrainedToWidth:size.width
                                                          numberOfLines:params.titleNumberOfLines
                                                                   font:params.titleFont
                                                          lineBreakMode:params.titleLineBreakMode];
    
    // image
    CGRect imageFrame = CGRectZero;
    imageFrame.size = (CGSizeEqualToSize(params.imageSizeToUseForLayout, (CGSize){HUGE_VALF, HUGE_VALF})) ?
                                                                    params.image.size : params.imageSizeToUseForLayout;
    
    imageFrame.origin.x = floorf((size.width - imageFrame.size.width)/2.0);
    
    const CGFloat kTitleLabelTopMargin = (titleFrame.size.height != 0) ? 3.0 : 0.0;
    const CGFloat kTitleLabelBottomMargin = (titleFrame.size.height != 0) ? 0.0 : 0.0;
    
    imageFrame.origin.y = floorf((size.height - (imageFrame.size.height + (kTitleLabelTopMargin +
                                                                           titleFrame.size.height +
                                                                           kTitleLabelBottomMargin
                                                                           )
                                                 )
                                  )/2.0);
    
    titleFrame.origin.x = floorf((size.width - titleFrame.size.width)/2.0);
    titleFrame.origin.y = imageFrame.origin.y + imageFrame.size.height + kTitleLabelTopMargin;
    
    layoutInfo.titleFrame = titleFrame;
    layoutInfo.imageFrame = imageFrame;
    CGFloat contingentRightWidth = MIN(MAX(titleFrame.size.width, imageFrame.size.width), size.width);
    CGFloat contingentRightHeight = titleFrame.origin.y + titleFrame.size.height + kTitleLabelBottomMargin - imageFrame.origin.y;
    layoutInfo.contingentRightSize = CGSizeMake(contingentRightWidth, contingentRightHeight);
    
    return layoutInfo;
}

- (KMKImageViewWithTitleLayoutParams *)layoutParams {
    KMKImageViewWithTitleLayoutParams *layoutParams = [KMKImageViewWithTitleLayoutParams new];
    layoutParams.image = self.imageView.image;
    layoutParams.imageSizeToUseForLayout = _imageSizeToUseForLayout;
    layoutParams.title = self.titleLabel.text;
    layoutParams.titleFont = self.titleLabel.font;
    layoutParams.titleLineBreakMode = self.titleLabel.lineBreakMode;
    layoutParams.titleNumberOfLines = self.titleLabel.numberOfLines;
    return layoutParams;
}

- (void)init_common {
    _imageView = [UIImageView new];
    [self addSubview:_imageView];
    self.imageSizeToUseForLayout = CGSizeMake(HUGE_VALF, HUGE_VALF);
    
    _titleLabel = [UILabel new];
    _titleLabel.textColor = [UIColor grayColor];
    _titleLabel.font = [UIFont systemFontOfSize:9.0];
    _titleLabel.backgroundColor = [UIColor clearColor];
    _titleLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:_titleLabel];
}

@end
