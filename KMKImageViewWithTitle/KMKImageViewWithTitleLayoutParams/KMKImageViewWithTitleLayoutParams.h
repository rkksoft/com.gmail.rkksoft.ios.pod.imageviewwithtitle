//  Copyright (c) 2015, Yauheni Klishevich.
//  Released under the MIT license.

#import <UIKit/UIKit.h>

@interface KMKImageViewWithTitleLayoutParams : NSObject

/**
 May be nil.
 */
@property (strong, nonatomic) UIImage *image;

/**
 Text of titleLabel.
 */
@property (strong, nonatomic) NSString *title;

/**
 Max number of lines in textLabel.
 */
@property (nonatomic, assign) NSInteger titleNumberOfLines;

/**
 Font of textLabel.
 */
@property (strong, nonatomic) UIFont *titleFont;

/**
 lineBreakMode of the first label.
 */
@property (nonatomic, assign) NSLineBreakMode titleLineBreakMode;

/**
 See comment to KMKImageViewWithTitle.imageSizeToUseForLayout property.
 */
@property (assign, nonatomic) CGSize imageSizeToUseForLayout;

@end
