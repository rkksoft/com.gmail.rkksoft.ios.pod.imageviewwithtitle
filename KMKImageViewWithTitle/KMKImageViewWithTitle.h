//  Copyright (c) 2015, Yauheni Klishevich.
//  Released under the MIT license.

#import <UIKit/UIKit.h>

#import "KMKImageViewWithTitleLayoutParams.h"

/**
 Title is located under the image.
 Layout is done with assumption that maximum number of lines for titleLabel is 1.
 */
@interface KMKImageViewWithTitle : UIView

#pragma mark Class methods

- (KMKImageViewWithTitleLayoutParams *)layoutParams;

#pragma mark Instance methods

/**
 Return the most appropriate size but not larger than the specified size.
 */
+ (CGSize)preferredSizeForDataSource: (KMKImageViewWithTitleLayoutParams *)params size: (CGSize)size;

@property (strong, nonatomic) UIImageView *imageView;

/**
 Font: "system, regular, 9pt" and with grayColor.
 */
@property (strong, nonatomic) UILabel *titleLabel;

/**
 If set then for size of image is used this value, otherwise size of image itself is used when making layout.
 titleLabel is supposed not to be included in value of this property.
 Default value {HUGE_VALF, HUGE_VALF} which is corresponding to making layout using image's size.
 */
@property (assign, nonatomic) CGSize imageSizeToUseForLayout;

@end
