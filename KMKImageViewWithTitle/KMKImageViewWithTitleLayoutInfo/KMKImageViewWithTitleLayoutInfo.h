//  Copyright (c) 2015, Yauheni Klishevich.
//  Released under the MIT license.

#import <UIKit/UIKit.h>

@interface KMKImageViewWithTitleLayoutInfo : NSObject

@property (nonatomic, assign) CGRect imageFrame;
@property (nonatomic, assign) CGRect titleFrame;

/**
 The size to fit content of view taking into account the specified layout params and max size (contingent).
 */
@property (nonatomic, assign) CGSize contingentRightSize;

@end
